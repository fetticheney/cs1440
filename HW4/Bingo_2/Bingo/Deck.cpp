//
// Created by Stephen Clyde on 2/16/17.
//

#include "Deck.h"

Deck::Deck(int cardSize, int cardCount, int numberMax) : m_cardSize(cardSize), m_cardCount(cardCount), m_numberMax(numberMax)
{
    // TODO: Implement
    for (int x = 0; x < m_cardCount; x++)
    {
        m_cards.push_back(new Card(m_cardSize, m_numberMax));
    }
    m_cards[cardCount]->resetholdID();
}

Deck::~Deck()
{
    // TODO: Implement
    for (unsigned int x = 0; x < m_cardCount - 1; x++)
    {
        delete m_cards[x];
    }
}

void Deck::print(std::ostream& out) const
{
    // TODO: Implement
    for (int i = 0; i < m_cardCount; i++) {
        std::cout << std::endl << std::endl << "Card #" << i + 1 << std::endl;

        for (int y = 0; y <= m_cardSize - 1; y++) {
            std::cout << "+----";
        }
        std::cout << "+" << std::endl;

        std:: cout << std::left;
        for (int x = 0; x <= m_cardSize - 1; x++) {

            for (int y = 0; y <= m_cardSize - 1; y++) {
                std::cout << "|";
                std::cout << std::setw(4) << m_cards[i]->getBingo(x, y);
            }
            std::cout << "|" << std::endl;

            for (int y = 0; y <= m_cardSize - 1; y++) {
                std::cout << "+----";
            }
            std::cout << "+" << std::endl;
        }
    }
}

void Deck::print(std::ostream& out, int cardIndex) const
{
    // TODO: Implement

    if (cardIndex > m_cardCount)
    {
        std::cout << "Invalid, requested card outside bounds of deck size." << std::endl;
        return;
    }

    std::cout << std::endl << std::endl << "Card #" << cardIndex << std::endl;

    for (int y = 0; y <= m_cardSize - 1; y++) {
        std::cout << "+----";
    }
    std::cout << "+" << std::endl;

    std:: cout << std::left;
    for (int x = 0; x <= m_cardSize - 1; x++) {

        for (int y = 0; y <= m_cardSize - 1; y++) {
            std::cout << "|";
            std::cout << std::setw(4) << m_cards[cardIndex - 1]->getBingo(x, y);
        }
        std::cout << "|" << std::endl;

        for (int y = 0; y <= m_cardSize - 1; y++) {
            std::cout << "+----";
        }
        std::cout << "+" << std::endl;
    }
}



