//
// Created by chase on 3/5/2017.
//

#ifndef BINGO_CARD_H
#define BINGO_CARD_H

#include <vector>
#include <algorithm>
#include <iostream>

class Card {
public:
    Card(int cardSize, int numberMax);
    int getBingo (int x, int y);
    int getID() const {return cardID;}
    int getSize() const {return n_cardSize;}
    int getMax() const {return n_numberMax;}
    void resetholdID() {holdID = 0;}

private:
    int n_cardSize;
    int n_numberMax;
    int** n_card;
    static unsigned int holdID;
    unsigned int cardID = 0;

};


#endif //BINGO_CARD_H
