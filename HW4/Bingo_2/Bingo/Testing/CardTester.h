//
// Created by chase on 3/5/2017.
//

#ifndef BINGO_CARDTESTER_H
#define BINGO_CARDTESTER_H

#include <iostream>
#include <vector>

class CardTester {
public:
    void testcardID01();
    void testcardID02();
    void testcardSize();
    void testmaxNumber();
    void checkDupe();
};


#endif //BINGO_CARDTESTER_H
