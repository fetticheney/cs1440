//
// Created by chase on 3/5/2017.
//

#include "DeckTester.h"
#include "../Deck.h"

void DeckTester::testcardSize() {
    std::cout << std::endl << "Test Suite: DeckTester::testcardSize" << std::endl;
    Deck *currentDeck = new Deck(12, 1, 100);
    if (currentDeck->getSize() != 12)
        std::cout << std::endl << "Error in retrieving correct card ID. Retrieved " << currentDeck->getSize()
                  << " expected 12.";
}
void DeckTester::testmaxNumber()
{
    std::cout << "Test Suite: DeckTester::testmaxNumber" << std::endl;
    Deck *currentDeck = new Deck(12, 1, 199);
    if (currentDeck->getSize() != 12)
        std::cout << std::endl << "Error in retrieving correct max bingo number. Retrieved " << currentDeck->getMax()
                  << " expected 199.";
}
