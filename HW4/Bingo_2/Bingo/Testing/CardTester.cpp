//
// Created by chase on 3/5/2017.
//

#include "CardTester.h"
#include "../Card.h"

void CardTester::testcardID01()
{
    std::cout << std::endl << "Test Suite: CardTester::testcardID01" << std::endl;

    std::vector <Card*> deck;
    for (int x = 0; x < 10; x++)
    {
        deck.push_back(new Card(10, 300));
    }
    deck[9]->resetholdID();

    if (deck[5]->getID() != 6)
    {
        std::cout << std::endl << "Error in retrieving correct card ID. Retrieved " << deck[5]->getID() << " expected 6,";
    }
}
void CardTester::testcardID02()
{
    std::cout << "Test Suite: CardTester::testcardID01" << std::endl;
    std::vector <Card*> deck;
    for (int x = 0; x < 10; x++)
    {
        deck.push_back(new Card(10, 300));
    }

    if (deck[9]->getID() != 10)
    {
        std::cout << std::endl << "Error in retrieving correct card ID. Retrieved " << deck[9]->getID() << " expected 10,";
    }
}
void CardTester::testcardSize()
{
    std::cout << "Test Suite: CardTester::testcardSize" << std::endl;

    std::vector <Card*> deck;
    for (int x = 0; x < 10; x++)
    {
        deck.push_back(new Card(15, 300));
    }
    deck[9]->resetholdID();

    if (deck[5]->getSize() != 15)
    {
        std::cout << std::endl << "Error in retrieving correct card size. Retrieved " << deck[5]->getID() << " expected 15,";
    }
}
void CardTester::testmaxNumber()
{
    std::cout << "Test Suite: CardTester::testmaxNumber" << std::endl;

    std::vector <Card*> deck;
    for (int x = 0; x < 10; x++)
    {
        deck.push_back(new Card(15, 300));
    }
    deck[9]->resetholdID();

    if (deck[5]->getMax() != 300)
    {
        std::cout << std::endl << "Error in retrieving correct max number. Retrieved " << deck[5]->getID() << " expected 300,";
    }
}
void CardTester::checkDupe()
{
    std::cout << "Test Suite: CardTester::checkDupe" << std::endl;

    std::vector <Card*> deck;
    for (int x = 0; x < 10; x++)
    {
        deck.push_back(new Card(15, 300));
    }
    deck[9]->resetholdID();

    bool dupecheck = 0;

    for (int i = 0; i < 10; i++) {
        for (int a = 0; a < 15; a++) {
            for (int b = 0; b < 15; b++) {
                for (int c = 0; c < 15; c++) {
                    for (int d = b + 1; d < 15; d++) {
                        if (deck[i]->getBingo(a,b) == deck[i]->getBingo(c,d)) dupecheck = 1;
                    }
                }
            }
        }
    }

    if (dupecheck) std::cout << std::endl << "Error in dupe check, duplicate bingo numbers found on card.";
}