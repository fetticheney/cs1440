//
// Created by Stephen Clyde on 2/16/17.
//

#ifndef BINGO_DECK_H
#define BINGO_DECK_H

#include "Card.h"
#include <ostream>
#include <iostream>
#include <iomanip>
#include <vector>

// TODO: Extend this definition as you see fit

class Deck {

public:
    Deck(int cardSize, int cardCount, int numberMax);
    ~Deck();

    void print(std::ostream& out) const;
    void print(std::ostream& out, int cardIndex) const;
    int getSize() const {return m_cardSize;}
    int getCount() const {return m_cardCount;}
    int getMax() const {return m_numberMax;}

private:
    std::vector <Card*> m_cards;
    int m_cardSize;
    int m_cardCount;
    int m_numberMax;
};

#endif //BINGO_DECK_H
