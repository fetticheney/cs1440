//
// Created by chase on 3/5/2017.
//

#include "Card.h"

unsigned int Card::holdID = 0;

Card::Card(int cardSize, int numberMax) :
        n_cardSize(cardSize), n_numberMax(numberMax)
{
    int rows = n_cardSize;
    int cols = n_cardSize;

    std::vector<int> randomGen;

    for (int i = 1; i < n_numberMax; i++)
    {
        randomGen.push_back(i);
    }
    std::random_shuffle(randomGen.begin(), randomGen.end());

    n_card = new int*[rows];
    for (int x = 0; x < rows; x++)
    {
        n_card[x] = new int[cols];
    }

    int token = 0;
    for (int x = 0; x < n_cardSize; x++)
    {
        for (int y = 0; y < n_cardSize; y++)
        {
            n_card[x][y] = randomGen[token];
            token++;
        }
    }

    holdID++;
    cardID = holdID;
}

int Card::getBingo (int x, int y)
{
    int ret = n_card[x][y];
    return ret;
}