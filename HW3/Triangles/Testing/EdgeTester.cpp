//
// Created by Stephen Clyde on 2/3/17.
//

#include "EdgeTester.h"

#include <iostream>
#include <cmath>
#include "../Point.h"
#include "../Edge.h"

void EdgeTester::testEdge01()
{
    std::cout << "Execute EdgeTester::testEdge01" << std::endl;

    Point p0(0,0,0);
    Point p1(1,0,0);

    // Create and test a edge between p0 and p1, then test all characteristics of that edge
    Edge e(&p0, &p1);
    if (!e.isValid() || e.getPoint1() != &p0 || e.getPoint2()!= &p1)
    {
        std::cout << "Failure in constructing Edge(&p1, &p2) isValid()="
                  << e.isValid()
                  << " point1=" << e.getPoint1() << " (expecting " << &p0 << ")"
                  << " point2=" << e.getPoint2() << " (expecting " << &p1 << ")"
                  << std::endl;
        return;
    }

    if (e.getLength()!=1)
    {
        std::cout << "Failure in e.getLenth(), length="
                  << e.getLength() << " (expecting 1)"
                  << std::endl;
        return;
    }

    if (e.getSlopeX()!=INFINITY)
    {
        std::cout << "Failure in e.getSlopeX(), slope="
                  << e.getSlopeX() << " (expecting INFINITY)"
                  << std::endl;
        return;
    }

    if (e.getSlopeY()!=0)
    {
        std::cout << "Failure in e.getSlopeY(), slope="
                  << e.getSlopeY() << " (expecting 0)"
                  << std::endl;
        return;
    }

    if (e.getSlopeZ()!=0)
    {
        std::cout << "Failure in e.getSlopeZ(), slope="
                  << e.getSlopeZ() << " (expecting 0)"
                  << std::endl;
        return;
    }
}

void EdgeTester::testEdge02()
{
    std::cout << "Execute EdgeTester::testEdge02" << std::endl;

    Point p0(1,0,0);
    Point p1(3,4,5);

    // Create and test a edge between p0 and p1, then test all characteristics of that edge
    Edge e(&p0, &p1);
    if (!e.isValid() || e.getPoint1() != &p0 || e.getPoint2()!= &p1)
    {
        std::cout << "Failure in constructing Edge(&p1, &p2) isValid()="
                  << e.isValid()
                  << " point1=" << e.getPoint1() << " (expecting " << &p0 << ")"
                  << " point2=" << e.getPoint2() << " (expecting " << &p1 << ")"
                  << std::endl;
        return;
    }

    if (fabs(e.getLength() - 6.708203932) > 0.001)
    {
        std::cout << "Failure in e.getLenth(), length="
                  << e.getLength() << " (expecting 6.708203932)"
                  << std::endl;
        return;
    }

    if (fabs(e.getSlopeX() - 0.312347524) >= 0.001)
    {
        std::cout << "Failure in e.getSlopeX(), slope="
                  << e.getSlopeX() << " (expecting 0.312347524)"
                  << std::endl;
        return;
    }

    if (fabs(e.getSlopeY() - 0.742781353) >= 0.001)
    {
        std::cout << "Failure in e.getSlopeY(), slope="
                  << e.getSlopeY() << " (expecting 0.742781353)"
                  << std::endl;
        return;
    }

    if (fabs(e.getSlopeZ() - 1.118033989) >= 0.001)
    {
        std::cout << "Failure in e.getSlopeZ(), slope="
                  << e.getSlopeZ() << " (expecting 1.118033989)"
                  << std::endl;
        return;
    }
}

void EdgeTester::testParallelEdges()
{
    std::cout << "Execute EdgeTester::testParallelEdges" << std::endl;

    // TODO: Writing a representative set of test cases for edges that are parallel with other

    Point p0(0,0,0);
    Point p1(5,10,0);
    Edge a(&p0, &p1);

    Point p2(5,10,0);
    Point p3(10,20,0);
    Edge b(&p2, &p3);

    if (!a.isParallelTo(b))
    {
        std::cout << "Unexpected failure in comparing slopes, should be parallel." << std::endl;
    }

}

void EdgeTester::testNonParallelEdges()
{
    std::cout << "Execute EdgeTester::testNonParallelEdges" << std::endl;

    // TODO: Writing a representative set of test cases for edges that are not parallel with other

    Point p0(0,0,0);
    Point p1(5,10,0);
    Edge a(&p0, &p1);

    Point p2(5,10,0);
    Point p3(10,29,0);
    Edge b(&p2, &p3);

    if (a.isParallelTo(b))
    {
        std::cout << "Unexpected failure in comparing slopes, should NOT be parallel." << std::endl;
    }

}

void EdgeTester::testNonLengthEdges()
{
    std::cout << "Execute EdgeTester::testNonLengthEdges" << std::endl;

    // TODO: Writing a representative set of test cases for edges have a length of zero or approximately zero

    Point p0(9,9,9);
    Point p1(9,9,9);
    Edge a(&p0, &p1);

    if (a.getLength() != 0)
    {
        std::cout << "Unexpected failure in comparing length, should be 0." << std::endl;
    }

    Point p2(9,9,9);
    Point p3(10,9,9);
    Edge b(&p2, &p3);


    if (b.getLength() == 0)
    {
        std::cout << "Unexpected failure in comparing length, should NOT be 0." << std::endl;
    }

}

void EdgeTester::testBadEdges()
{
    std::cout << "Execute EdgeTester::testBadEdges" << std::endl;

    // TODO: Writing a representative set of test cases for edges not formed correctly

    Point p0(7,4,3);
    Point p1(17,6,2);
    Edge a(&p0, &p1);

    if (a.getLength() - 10.246951 >= 0.001)
    {
        std::cout << "Unexpected failure in testing length, expected 10.246951." << std::endl;
    }

    if (a.getSlopeX() - 4.47214 >= 0.001)
    {
        std::cout << "Unexpected failure in testing X slope, expected 4.47214." << std::endl;
    }

    if (a.getSlopeY() - 0.19900 >= 0.001)
    {
        std::cout << "Unexpected failure in testing Y slope, expected 0.19900." << std::endl;
    }

    if (a.getSlopeZ() - -0.09805 >= 0.001)
    {
        std::cout << "Unexpected failure in testing Z slope, expected -0.09805." << std::endl;
    }

}
