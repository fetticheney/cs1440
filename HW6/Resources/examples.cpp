#include <iostream>

//TEMPLATES

//syntax
template <class type> ret-type func-name(parameter list){
	//body of function
}

//example
template <typename X> X const& Min (X const& a, X const& b){
	return a > b ? b:a;
}

int main ()
{
	int x = 30;
	int y = 25;
	std::cout << "Min(x, y): " << Min(x,y) << std::endl;
}