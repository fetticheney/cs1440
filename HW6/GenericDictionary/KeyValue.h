//
// Created by chase on 4/5/2017.
//

#ifndef GENERICDICTIONARY_KEYVALUE_H
#define GENERICDICTIONARY_KEYVALUE_H


template <class T1, class T2>
class KeyValue
{
    public:
        KeyValue(T1, T2);
        KeyValue();
        KeyValue(const KeyValue &);
        KeyValue& operator=(const KeyValue<T1, T2>& obj);
        T1 getKey() const {return m_key;}
        T2 getValue() const {return m_value;}
        //void setKey(T1 key) {m_key = key;}
        //void setValue(T2 value) {m_value = value;}

    private:
        T1 m_key;
        T2 m_value;
};


template <class T1, class T2>
KeyValue<T1,T2>::KeyValue(T1 key, T2 value)
{
    m_key = key;
    m_value = value;
}

template <class T1, class T2>
KeyValue<T1,T2>::KeyValue()
{

}

template <class T1, class T2>
KeyValue<T1,T2>& KeyValue<T1,T2>::operator=(const KeyValue<T1, T2>& obj)
{
    m_key = obj.m_key;
    m_value = obj.m_value;
    return *this;
}

template <class T1, class T2>
KeyValue<T1,T2>::KeyValue(const KeyValue &obj)
{
    m_key = obj.m_key;
    m_value = obj.m_value;
}

#endif //GENERICDICTIONARY_KEYVALUE_H
