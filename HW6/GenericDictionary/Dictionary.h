//
// Created by chase on 4/5/2017.
//

#ifndef GENERICDICTIONARY_DICTIONARY_H
#define GENERICDICTIONARY_DICTIONARY_H

#include <vector>
#include <iostream>
#include "KeyValue.h"

template <class T1, class T2>
class Dictionary
{
    public:
        Dictionary();
        Dictionary(int);
        Dictionary(const Dictionary &);
        ~Dictionary();
        Dictionary& operator=(const Dictionary<T1, T2>& obj);
        const KeyValue<T1, T2>& operator[](int index);
        void add(T1, T2);
        int getCount() const {return m_count;}
        int getSize() const {return m_size;}
        const KeyValue<T1, T2> getByKey(T1);
        const KeyValue<T1, T2> getByIndex(int n);
        void removeByKey(T1);
        void removeByIndex(int n);

    private:
        int m_size;
        int m_count;
        std::vector < KeyValue<T1, T2>* > m_kv;
};

template <class T1, class T2>
Dictionary<T1,T2>::Dictionary()
{
    m_size = 10;
    m_kv.reserve(m_size);
    m_count = 0;
    //std::cout << ">>Default<<" << std::endl;
}

template <class T1, class T2>
Dictionary<T1,T2>::Dictionary(int size)
{
    m_size = size;
    m_kv.reserve(m_size);
    m_count = 0;
    //std::cout << ">>Param<<" << std::endl;
}

template <class T1, class T2>
Dictionary<T1,T2>::Dictionary(const Dictionary &obj)
{
    m_size = obj.m_size;
    m_count = obj.m_count;
    m_kv = obj.m_kv;
}

template <class T1, class T2>
Dictionary<T1,T2>::~Dictionary()
{
    for (unsigned int i = 0; i < m_kv.size(); i++)
    {
        delete m_kv[i];
    }
    m_kv.clear();
}

template <class T1, class T2>
Dictionary<T1,T2>& Dictionary<T1,T2>::operator=(const Dictionary<T1, T2>& obj)
{
    m_size = obj.m_size;
    m_count = obj.m_count;
    m_kv = obj.m_kv;
    return *this;
}

template <class T1, class T2>
void Dictionary<T1,T2>::add(T1 key, T2 value)
{
    KeyValue<T1, T2>* new_kv = new KeyValue<T1, T2>(key, value);
    m_kv.push_back(new_kv);
    m_count++;
    //std::cout << m_kv[m_count - 1]->getValue() << std::endl;
}

template <class T1, class T2>
const KeyValue<T1,T2> Dictionary<T1,T2>::getByKey(T1 key)
{
    //std::cout << "Searching for key: " << key << std::endl;
    for (int i = 0; i < m_kv.size(); i++)
    {
        //std::cout << "Comparing to key: " << m_kv[i]->getKey() << std::endl;
        if (m_kv[i]->getKey() == key)
        {
            //std::cout << "match found" << std::endl;
            KeyValue<T1,T2> *rt = m_kv[i];
            return *rt;
        }
    }
        throw std::string("Error, no match for string in getByKey found.");
}

template <class T1, class T2>
const KeyValue<T1,T2> Dictionary<T1,T2>::getByIndex(int n)
{
    if (n > m_count - 1 || n < 0)
    {
        throw std::string("Error, no match for index in getByKey found.");
    }
    KeyValue<T1,T2> *rt = m_kv[n];
    return *rt;
}

template <class T1, class T2>
const KeyValue<T1,T2>& Dictionary<T1,T2>::operator[](int n)
{
    if (n > m_count - 1 || n < 0)
    {
        throw std::string("Error, no match for index in getByKey found.");
    }
    return *m_kv[n];
}

template <class T1, class T2>
void Dictionary<T1,T2>::removeByKey(T1 key)
{
    //std::cout << "Searching for key: " << key << std::endl;
    for (int i = 0; i < m_kv.size(); i++)
    {
        //std::cout << "Comparing to key: " << m_kv[i]->getKey() << std::endl;
        if (m_kv[i]->getKey() == key)
        {
            //std::cout << "match found" << std::endl;
            m_kv.erase(m_kv.begin()+i);
            m_count--;
            return;
        }
    }
    throw std::string("Error, no match for key in removeByKey found.");
}

template <class T1, class T2>
void Dictionary<T1,T2>::removeByIndex(int n)
{
    if (n > m_count - 1 || n < 0)
    {
        throw std::string("Error, no match for index in getByKey found.");
    }
    m_kv.erase(m_kv.begin()+n);
    m_count--;
    return;
}
#endif //GENERICDICTIONARY_DICTIONARY_H
