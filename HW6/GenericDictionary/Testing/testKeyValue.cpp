//
// Created by chase on 4/6/2017.
//

#include "testKeyValue.h"

void testKeyValue::testgetKey()
{
    Dictionary<std::string, std::string> myDictionary(1);
    myDictionary.add("hair color", "brown");
    if (myDictionary[0].getKey() != "hair color")
    {
        std::cout << "getKey() failure - mismatch in m_key, should be 'hair color' - actual: "
                  << myDictionary[0].getKey() << std::endl;
    }
}

void testKeyValue::testgetValue()
{
    Dictionary<std::string, std::string> myDictionary(1);
    myDictionary.add("hair color", "brown");
    if (myDictionary[0].getValue() != "brown")
    {
        std::cout << "getValue() failure - mismatch in m_key, should be 'brown' - actual: "
                  << myDictionary[0].getValue() << std::endl;
    }
}

/*
void testKeyValue::testsetKey()
{

}

void testKeyValue::testsetValue()
{

}
 */