//
// Created by chase on 4/6/2017.
//

#ifndef GENERICDICTIONARY_TESTKEYVALUE_H
#define GENERICDICTIONARY_TESTKEYVALUE_H

#include "../Dictionary.h"
#include "../KeyValue.h"

class testKeyValue {

    public:
        void testgetKey();
        void testgetValue();
        //void testsetKey();
        //void testsetValue();

};


#endif //GENERICDICTIONARY_TESTKEYVALUE_H
