//
// Created by chase on 4/6/2017.
//

#include "testDictionary.h"

void testDictionary::testdefaultConstructor()
{
    Dictionary<std::string, std::string> myDictionary;
    int a = myDictionary.getSize();
    if (a != 10)
    {
        std::cout << "Dictionary default constructor failure - mismatch in m_count, should be 10 - actual: "
                  << a << std::endl;
    }
}

void testDictionary::testparamaterizedConstructor()
{
    Dictionary<std::string, std::string> myDictionary(1000);
    int a = myDictionary.getSize();
    if (a != 1000)
    {
        std::cout << "Dictionary(int size) constructor failure - mismatch in m_count, should be 10 - actual: "
                  << a << std::endl;
    }
}

void testDictionary::testoverloadedSubscript()
{
    Dictionary<std::string, std::string> myDictionary(3);
    myDictionary.add("hair color", "brown");
    myDictionary.add("car", "Ford");
    myDictionary.add("cheese", "3");
    const KeyValue<std::string, std::string>& kv = myDictionary[2];
    if (kv.getKey() != "cheese" && kv.getValue() != "3")
    {
        std::cout << "operator[] failure - mismatch in value of myDictionary[2], should be 3 - actual: "
                  << kv.getValue() << std::endl;
    }
}

void testDictionary::testadd()
{
    Dictionary<std::string, std::string> myDictionary(3);
    myDictionary.add("hair color", "brown");
    myDictionary.add("car", "Ford");
    myDictionary.add("cheese", "3");
    if (myDictionary.getCount() != 3 && myDictionary[1].getValue() != "Ford")
    {
        std::cout << "testadd() failure - m_count and value of myDictionary[1] incorrect" << std::endl;
    }
}

void testDictionary::testgetCount()
{
    Dictionary<std::string, std::string> myDictionary(5);
    myDictionary.add("hair color", "brown");
    myDictionary.add("car", "Ford");
    myDictionary.add("cheese", "3");
    myDictionary.add("vegetable", "Broccoli");
    myDictionary.add("place", "Canada");
    if (myDictionary.getCount() != 5)
    {
        std::cout << "getCount() failure - mismatch in m_count, should be 5 - actual: "
                  << myDictionary.getCount() << std::endl;
    }

}

void testDictionary::testgetByKey()
{
    Dictionary<std::string, std::string> myDictionary(5);
    myDictionary.add("hair color", "brown");
    myDictionary.add("car", "Ford");
    myDictionary.add("cheese", "3");
    myDictionary.add("vegetable", "Broccoli");
    myDictionary.add("place", "Canada");
    KeyValue<std::string, std::string> vegetable = myDictionary.getByKey("vegetable");
    if (vegetable.getValue() != "Broccoli")
    {
        std::cout << "getByKey() failure - mismatch in m_value, should be Broccoli - actual: "
                  << vegetable.getValue() << std::endl;
    }
}

void testDictionary::testgetByIndex1()
{
    Dictionary<std::string, std::string> myDictionary(5);
    myDictionary.add("hair color", "brown");
    myDictionary.add("car", "Ford");
    myDictionary.add("cheese", "3");
    myDictionary.add("vegetable", "Broccoli");
    myDictionary.add("place", "Canada");
    const KeyValue<std::string, std::string>& kv = myDictionary.getByIndex(4);

    if (kv.getValue() != "Canada")
    {
        std::cout << "getByIndex() failure - mismatch in m_value, should be Canada - actual: "
                  << kv.getValue() << std::endl;
    }
}

void testDictionary::testgetByIndex2()
{
    Dictionary<std::string, std::string> myDictionary(5);
    myDictionary.add("hair color", "brown");
    myDictionary.add("car", "Ford");
    myDictionary.add("cheese", "3");
    myDictionary.add("vegetable", "Broccoli");
    myDictionary.add("place", "Canada");
    const KeyValue<std::string, std::string>& kv = myDictionary[2];

    if (kv.getValue() != "3")
    {
        std::cout << "getByIndex() failure - mismatch in m_value, should be 3 - actual: "
                  << kv.getValue() << std::endl;
    }
}

void testDictionary::testremoveByKey()
{
    Dictionary<std::string, std::string> myDictionary(5);
    myDictionary.add("hair color", "brown");
    myDictionary.add("car", "Ford");
    myDictionary.add("cheese", "3");
    myDictionary.add("vegetable", "Broccoli");
    myDictionary.add("place", "Canada");
    myDictionary.removeByKey("cheese");

    try
    {
        KeyValue<std::string, std::string> cheese = myDictionary.getByKey("cheese");
        std::cout << "Error, cheese was not deleted" << std::endl;
    }
    catch (std::string exceptionString)
    {
        //std::cout << exceptionString << std::endl;
    }
}

void testDictionary::testremoveByIndex()
{
    Dictionary<std::string, std::string> myDictionary(5);
    myDictionary.add("hair color", "brown");
    myDictionary.add("car", "Ford");
    myDictionary.add("cheese", "3");
    myDictionary.add("vegetable", "Broccoli");
    myDictionary.add("place", "Canada");
    myDictionary.removeByIndex(2);

    try
    {
        KeyValue<std::string, std::string> cheese = myDictionary.getByKey("cheese");
        std::cout << "Error, cheese was not deleted" << std::endl;
    }
    catch (std::string exceptionString)
    {
        //std::cout << exceptionString << std::endl;
    }
}