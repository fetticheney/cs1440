//
// Created by chase on 4/6/2017.
//

#ifndef GENERICDICTIONARY_TESTDICTIONARY_H
#define GENERICDICTIONARY_TESTDICTIONARY_H

#include "../Dictionary.h"
#include "../KeyValue.h"

class testDictionary {

    public:
        void testdefaultConstructor();
        void testparamaterizedConstructor();
        void testoverloadedSubscript();
        void testadd();
        void testgetCount();
        void testgetByKey();
        void testgetByIndex1();
        void testgetByIndex2();
        void testremoveByKey();
        void testremoveByIndex();

};


#endif //GENERICDICTIONARY_TESTDICTIONARY_H
