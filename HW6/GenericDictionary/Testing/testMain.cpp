//
// Created by chase on 4/5/2017.
//

#include <iostream>
#include "testDictionary.h"
#include "testKeyValue.h"

int main()
{
    std::cout << "Execute Test Cases" << std::endl << std::endl;

    std::cout << "Testing Dictionary.." << std::endl << std::endl;
    testDictionary dictionaryTester;
    dictionaryTester.testdefaultConstructor();
    dictionaryTester.testparamaterizedConstructor();
    dictionaryTester.testoverloadedSubscript();
    dictionaryTester.testadd();
    dictionaryTester.testgetCount();
    dictionaryTester.testgetByKey();
    dictionaryTester.testgetByIndex1();
    dictionaryTester.testgetByIndex2();
    dictionaryTester.testremoveByKey();
    dictionaryTester.testremoveByIndex();

    std::cout << "Testing KeyValue.." << std::endl << std::endl;
    testKeyValue keyvalueTester;
    keyvalueTester.testgetKey();
    keyvalueTester.testgetValue();
    //keyvalueTester.testsetKey();
    //keyvalueTester.testsetValue();
}