#include <iostream>
#include <string>
#include <cstring>
#include "Dictionary.h"


int main()
{
    Dictionary<std::string, std::string> yourDictionary(20);
    std::cout << "Add key/value pairs to your dictionary now. Type 'done' when finished" << std::endl;
    std::string input1 = "begin";
    std::string input2 = "begin";

    while(1) {
        std::cout << "key: ";
        std::cin >> input1;
        if (input1 == "done") break;
        std::cout << "value: ";
        std::cin >> input2;
        yourDictionary.add(input1, input2);
    }

    std::cout << std::endl << "You entered: " << yourDictionary.getCount() << " items into your dictionary." << std::endl;
    std::cout << std::endl << "Here are the contents of your dictionary:" << std::endl << std::endl;
    for (int i = 0; i < yourDictionary.getCount(); i++)
    {
        std::cout << yourDictionary[i].getKey() << "\t\t";
        std::cout << yourDictionary[i].getValue() << std::endl;
    }

    std::cout << std::endl << "Enter a key to search for: ";
    std::string input3;
    std::cin >> input3;

    try
    {
        KeyValue<std::string, std::string> findKey;
        findKey = yourDictionary.getByKey(input3);
        std::cout << "Value for key: " << input3 << " found : " << findKey.getValue() << std::endl;
    }
    catch (std::string exceptionString)
    {
        std::cout << exceptionString << std::endl;
    }

    std::cout << std::endl << "Enter an index to search for: ";
    int input4;
    std::cin >> input4;

    try
    {
        KeyValue<std::string, std::string> findIndex;
        findIndex = yourDictionary.getByIndex(input4);
        std::cout << "Value for index: " << input4 << " found : " << findIndex.getValue() << std::endl;
    }
    catch (std::string exceptionString)
    {
        std::cout << exceptionString << std::endl;
    }

    std::cout << std::endl << "Enter a key to delete: ";
    std::string input5;
    std::cin >> input5;

    try
    {
        yourDictionary.removeByKey(input5);
        std::cout << "Key/Value pair for key " << input5 << " successfully deleted." << std::endl;
    }
    catch (std::string exceptionString)
    {
        std::cout << exceptionString << std::endl;
    }

    std::cout << std::endl << "Enter an index to delete: ";
    int input6;
    std::cin >> input6;

    try
    {
        yourDictionary.removeByIndex(input6);
        std::cout << "Key/Value pair for index " << input6 << " successfully deleted." << std::endl;
    }
    catch (std::string exceptionString)
    {
        std::cout << exceptionString << std::endl;
    }



/*
    Dictionary<std::string, std::string> myDictionary(10);
    Dictionary<std::string, std::string> abc;

    int b = 0;
    b = abc.getCount();
    std::cout << "Count = " << b << std::endl;

    myDictionary.add("hair color", "brown");
    myDictionary.add("car", "Ford");
    myDictionary.add("cheese", "3");

    int a = 0;
    a = myDictionary.getCount();
    std::cout << "Count = " << a << std::endl;

    KeyValue<std::string, std::string> hairColor = myDictionary.getByKey("hair color");

    try
    {
        KeyValue<std::string, std::string> car;
        car = myDictionary.getByKey("car");
    }
    catch (std::string exceptionString)
    {
        std::cout << exceptionString << std::endl;
    }

    try
    {
        const KeyValue<std::string, std::string>& kv = myDictionary.getByIndex(2);
    }
    catch (std::string exceptionString)
    {
        std::cout << exceptionString << std::endl;
    }

    try
    {
        myDictionary.removeByKey("cheese");
    }
    catch (std::string exceptionString)
    {
        std::cout << exceptionString << std::endl;
    }
    std::cout << myDictionary.getCount() << std::endl;

    try
    {
        myDictionary.removeByIndex(1);
    }
    catch (std::string exceptionString)
    {
        std::cout << exceptionString << std::endl;
    }
    std::cout << myDictionary.getCount() << std::endl;

    try
    {
        myDictionary.removeByIndex(0);
    }
    catch (std::string exceptionString)
    {
        std::cout << exceptionString << std::endl;
    }
    std::cout << myDictionary.getCount() << std::endl;

    try
    {
        myDictionary.removeByIndex(0);
    }
    catch (std::string exceptionString)
    {
        std::cout << exceptionString << std::endl;
    }
    std::cout << myDictionary.getCount() << std::endl;
*/
    return 0;
}