#include "Analyzer.h"
#include "Configuration.h"
#include "DenialOfServiceAnalyzer.h"
#include "PortScanAnalyzer.h"
#include "ResultSet.h"

#include <fstream>
int main()
{
    Configuration testConfig;
    testConfig.addPair("A", "hotdog");
    testConfig.addPair("B", "hamburger");
    testConfig.addPair("C", "watermelon");
    testConfig.addPair("D", "12345");
    testConfig.addPair("E", "25.999");
    std::string testretStr1 = testConfig.retStr("A");
    std::string testretStr2 = testConfig.retStr("B");
    std::string testretStr3 = testConfig.retStr("C");
    int testretInt1 = testConfig.retInt("D");
    double testretDbl1 = testConfig.retDbl("E");

    std::cout << "Running Configuration Example:" << std::endl;
    std::cout << testretStr1 << std::endl;
    std::cout << testretStr2 << std::endl;
    std::cout << testretStr3 << std::endl;
    std::cout << testretInt1 << std::endl;
    std::cout << testretDbl1 << std::endl;
    std::cout << std::endl << std::endl;



    std::cout << "Running DDOS Example:" << std::endl;
    Configuration ddosConfig;
    ddosConfig.addPair("Timeframe", "2000");
    ddosConfig.addPair("Likely Attack Message Count", "200");
    ddosConfig.addPair("Possible Attack Message Count", "100");
    DenialOfServiceAnalyzer ddos1(ddosConfig);

    std::ifstream ipData;
    ipData.open("SampleData.csv");
    ResultSet test1 = ddos1.run(ipData);
    test1.print(std::cout);


    std::cout << "\n\n\nRunning Port Attack Example:" << std::endl;
    Configuration portConfig;
    portConfig.addPair("Likely Attack Port Count", "50");
    portConfig.addPair("Possible Attack Port Count", "25");
    PortScanAnalyzer port1(portConfig);

    std::ifstream portData;
    portData.open("SampleData.csv");
    ResultSet test2 = port1.run(portData);
    test2.print(std::cout);


    system("PAUSE");
    return 0;
}