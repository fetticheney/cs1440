//
// Created by chase on 4/22/2017.
//
#include "ResultSet.h"
#include <iostream>

#ifndef ITAK_ANALYZER_H
#define ITAK_ANALYZER_H


class Analyzer
{
public:
    virtual ResultSet run(std::istream&);
private:

};

ResultSet Analyzer::run(std::istream& in)
{

}

#endif //ITAK_ANALYZER_H
