//
// Created by chase on 4/22/2017.
//
#include <iostream>
#include <sstream>
#include <vector>
#include "ResultSet.h"
#include "Configuration.h"
#include "Analyzer.h"

#ifndef ITAK_PORTSCANANALYZER_H
#define ITAK_PORTSCANANALYZER_H

class PortScanAnalyzer : public Analyzer
{
private:
    int likelyThreshold;
    int possibleThreshold;
    std::map < std::string, std::vector < int > > load;
    ResultSet results;

public:
    PortScanAnalyzer(Configuration config);
    ResultSet run(std::istream&);

};

PortScanAnalyzer::PortScanAnalyzer(Configuration config)
{
    if (config.retInt("Likely Attack Port Count") != -1 &&
        config.retInt("Possible Attack Port Count") != -1)
    {
        likelyThreshold = config.retInt("Likely Attack Port Count");
        possibleThreshold = config.retInt("Possible Attack Port Count");
    }
    else
    {
        std::cout << "\nError - invalid configuration given to Port Scan Analyzer.\n";
    }
}

ResultSet PortScanAnalyzer::run(std::istream& in)
{
    std::string ip;
    std::string port;

    //std::map < std::string, std::vector < int > > load;

    while (!in.eof())
    {
        in.ignore(200, ',');
        std::getline(in, ip, ',');
        in.ignore(200, ',');
        std::getline(in, port);
        bool unique = true;

        for(int i = 0; i < load[ip].size(); i++)
        {
            if (load[ip][i] == stoi(port))
            {
                unique = false;
                break;
            }
        }

        if(unique)
        {
            load[ip].push_back(stoi(port));
        }
    }

    std::vector<std::string> likelyAttackers;
    std::vector<std::string> possibleAttackers;
    std::vector<std::string> portCount;

    for (auto it = load.begin(); it != load.end(); it++)
    {
        int c = it->second.size();

        if (c >= likelyThreshold)
        {
            bool unique = true;
            for (int i = 0; i < likelyAttackers.size(); i++)
            {
                if (likelyAttackers[i] == it->first)
                {
                    unique = false;
                    break;
                }
            }
            if (unique)
            {
                likelyAttackers.push_back(it->first);
            }
        }

        else if (c >= possibleThreshold)
        {
            bool unique = true;
            for (int i = 0; i < possibleAttackers.size(); i++)
            {
                if (possibleAttackers[i] == it->first)
                {
                    unique = false;
                    break;
                }
            }
            if (unique)
            {
                possibleAttackers.push_back(it->first);
            }
        }
    }

    portCount.push_back(std::to_string(likelyThreshold));
    portCount.push_back(std::to_string(possibleThreshold));

    results.addResult("Likely Attackers", likelyAttackers);
    results.addResult("Possible Attackers", possibleAttackers);
    results.addResult("Port Count", portCount);
    return results;
}
#endif //ITAK_PORTSCANANALYZER_H
