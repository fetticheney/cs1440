//
// Created by chase on 4/22/2017.
//

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "../DenialOfServiceAnalyzer.h"
#include "../PortScanAnalyzer.h"

void configTest1()
{
    //Testing successful configuration entry - returning as string.
    std::cout << "ConfigTest1::Adding String Pair - Returning as String" << std::endl;
    Configuration config;
    config.addPair("Nuclear Launch Code", "3aYt917bdFz");
    config.addPair("Condiment", "mustard");
    config.addPair("Chase", "Cheney");
    std::string test = config.retStr("Nuclear Launch Code");

    if (test != "3aYt917bdFz")
    {
        std::cout << "ConfigTest1 failure, expected '3aYt917bdFz' actual: " << test << std::endl;
    }
}

void configTest2()
{
    //Testing successful configuration entry - returning as integer
    std::cout << "ConfigTest2::Adding String Pair - Returning as Int" << std::endl;
    Configuration config;
    config.addPair("OneHundred", "100");
    config.addPair("TwoThousand", "2000");
    config.addPair("NineThousandNineHundred", "9900");
    int test = config.retInt("NineThousandNineHundred");

    if (test != 9900)
    {
        std::cout << "ConfigTest2 failure, expected '9900' actual: " << test << std::endl;
    }
}

void configTest3()
{
    //Testing successful configuration entry - returning as integer
    std::cout << "ConfigTest3::Adding String Pair - Returning as Double" << std::endl;
    Configuration config;
    config.addPair("OneHundred", "100");
    config.addPair("TwoThousand", "2000");
    config.addPair("Pi", "3.14159265");
    double test = config.retDbl("Pi");

    if (test != 3.14159265)
    {
        std::cout << "ConfigTest3 failure, expected '3.14159265' actual: " << test << std::endl;
    }
}

void resultTest1()
{
    //Testing successful ResultSet instance - should print the Mario Bros.
    std::cout << "resultTest1::Creating ResultSet of the Mario Bros" << std::endl;
    ResultSet test;
    std::string key;
    std::vector<std::string> value;
    key = "Mario Bros";
    value.push_back("Mario");
    value.push_back("Luigi");
    value.push_back("Peach");
    value.push_back("Bowser");
    value.push_back("Wario");
    value.push_back("Waluigi");
    test.addResult(key, value);
    std::cout << "\nPrinting out the Mario Bros:";
    test.print(std::cout);
    std::cout <<"\n\n";
}

void ddosTest1()
{
    std::cout << "Running DDOS Example:" << std::endl;
    Configuration ddosConfig;
    ddosConfig.addPair("Timeframe", "2000");
    ddosConfig.addPair("Likely Attack Message Count", "200");
    ddosConfig.addPair("Possible Attack Message Count", "100");
    DenialOfServiceAnalyzer ddos1(ddosConfig);

    std::ifstream ipData;
    ipData.open("../SampleData.csv");
    ResultSet test1 = ddos1.run(ipData);
    test1.print(std::cout);
}

void portTest1()
{
    std::cout << "\n\n\nRunning Port Attack Example:" << std::endl;
    Configuration portConfig;
    portConfig.addPair("Likely Attack Port Count", "50");
    portConfig.addPair("Possible Attack Port Count", "25");
    PortScanAnalyzer port1(portConfig);

    std::ifstream portData;
    portData.open("../SampleData.csv");
    ResultSet test2 = port1.run(portData);
    test2.print(std::cout);

}

int main()
{
    std::cout << "Execute Test Cases" << std::endl << std::endl;
    configTest1();
    configTest2();
    configTest3();
    resultTest1();
    ddosTest1();
    portTest1();
    system("PAUSE");
}
