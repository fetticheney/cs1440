//
// Created by chase on 4/22/2017.
//
#include <map>
#include <string>
#include <iostream>

#ifndef ITAK_CONFIGURATION_H
#define ITAK_CONFIGURATION_H

class Configuration
{
private:
    std::map < std::string, std::string > config;
public:
    Configuration();
    ~Configuration();
    void addPair(std::string, std::string);
    std::string retStr(std::string);
    int retInt(std::string);
    double retDbl(std::string);
};

Configuration::Configuration()
{

}

Configuration::~Configuration()
{

}

void Configuration::addPair(std::string key, std::string value)
{
    config[key] = value;
}

std::string Configuration::retStr(std::string key)
{
    if (config.count(key) != 0)
    {
        return config[key];
    }
    else
    {
        std::cout << "\nError, could not return value from config - entered key invalid.\n";
        return "";
    }
}

int Configuration::retInt(std::string key)
{
    if (config.count(key) != 0)
    {
        int ret = stoi(config[key]);
        return ret;
    }
    else
    {
        std::cout << "\nError, could not return value from config - entered key invalid.\n";
        return -1;
    }
}

double Configuration::retDbl(std::string key)
{
    if (config.count(key) != 0)
    {
        double ret = stod(config[key]);
        return ret;
    }
    else
    {
        std::cout << "\nError, could not return value from config - entered key invalid.\n";
        return -1;
    }
}

#endif //ITAK_CONFIGURATION_H
