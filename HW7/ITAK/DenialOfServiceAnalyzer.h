//
// Created by chase on 4/22/2017.
//
#include <vector>
#include <iostream>
#include <sstream>
#include "ResultSet.h"
#include "Configuration.h"
#include "Analyzer.h"

#ifndef ITAK_DENIALOFSERVICEANALYZER_H
#define ITAK_DENIALOFSERVICEANALYZER_H

class DenialOfServiceAnalyzer : public Analyzer
{
private:
    int timeframe;
    int likelyThreshold;
    int possibleThreshold;
    std::map < std::string, std::map <std::string, int> > load;
    ResultSet results;

public:
    DenialOfServiceAnalyzer(Configuration config);
    ResultSet run(std::istream&);

};

DenialOfServiceAnalyzer::DenialOfServiceAnalyzer(Configuration config)
{
    if (config.retInt("Timeframe") != -1 &&
        config.retInt("Likely Attack Message Count") != -1 &&
        config.retInt("Possible Attack Message Count") != -1)
    {
        timeframe = config.retInt("Timeframe");
        likelyThreshold = config.retInt("Likely Attack Message Count");
        possibleThreshold = config.retInt("Possible Attack Message Count");
    }
    else
    {
        std::cout << "\nError - invalid configuration given to DDOS Analyzer.\n";
    }
}

ResultSet DenialOfServiceAnalyzer::run(std::istream& in)
{
    std::string ip;
    std::string timestamp;

    while (!in.eof())
    {
        std::getline(in, timestamp, ',');
        std::getline(in, ip, ',');
        in.ignore(200,'\n');

        if (load.count(ip) == 0)
        {
            std::map <std::string, int> a;
            a[timestamp] = 0;
            load[ip] = a;
        }

        load[ip][timestamp] += 1;
    }

    //std::map < std::string, std::map <std::string, int> > load;
    std::vector<std::string> likelyAttackers;
    std::vector<std::string> possibleAttackers;
    std::vector<std::string> attackPeriods;
    std::vector<std::string> res_timeFrame;
    res_timeFrame.push_back(std::to_string(timeframe));
    std::string window;

    for (auto it = load.begin(); it != load.end(); it++)
    {
        for (auto it2 = load[it->first].begin(); it2 != load[it->first].end(); ++it2)
        {
            std::string s = it2->first;
            int c = it2->second;
            auto p = it2;
            p++;

            while(p != load[it->first].end() && stoi(p->first) < stoi(s) + timeframe)
            {
                c += p->second;
                p++;
            }

            if (c >= likelyThreshold)
            {
                bool unique = true;
                for (int i = 0; i < likelyAttackers.size(); i++)
                {
                    if (likelyAttackers[i] == it->first)
                    {
                        unique = false;
                        break;
                    }
                }

                if (unique)
                {
                    likelyAttackers.push_back(it->first);
                    window = "";
                    window += s;
                    window += " - ";
                    std::string endtime = std::to_string(stoi(s) + timeframe);
                    window += endtime;
                    attackPeriods.push_back(window);
                    //std::cout << it->first << std::endl;
                    //std::cout << window << std::endl;
                }
            }
            else if (c >= possibleThreshold)
            {
                bool unique = true;
                for (int i = 0; i < possibleAttackers.size(); i++)
                {
                    if (possibleAttackers[i] == it->first)
                    {
                        unique = false;
                        break;
                    }
                }

                if(unique)
                {
                    possibleAttackers.push_back(it->first);
                    window = "";
                    window += s;
                    window += " - ";
                    std::string endtime = std::to_string(stoi(s) + timeframe);
                    window += endtime;
                    attackPeriods.push_back(window);
                    //std::cout << it->first << std::endl;
                    //std::cout << window << std::endl;
                }
            }

        }
    }

    results.addResult("Likely Attackers", likelyAttackers);
    results.addResult("Possible Attackers", possibleAttackers);
    results.addResult("Attack Periods", attackPeriods);
    results.addResult("Timeframe", res_timeFrame);
    return results;
}

#endif //ITAK_DENIALOFSERVICEANALYZER_H
