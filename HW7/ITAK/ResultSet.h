//
// Created by chase on 4/22/2017.
//
#include <iostream>
#include <vector>
#include <string>
#include <utility>

#ifndef ITAK_RESULTSET_H
#define ITAK_RESULTSET_H

class ResultSet
{
private:
    std::vector< std::pair < std::string, std::vector<std::string> > > m_result;
public:
    ResultSet();
    void addResult(std::string, std::vector<std::string>);
    void print(std::ostream&);

};

ResultSet::ResultSet()
{

}

void ResultSet::addResult(std::string result, std::vector<std::string> container)
{
    std::pair<std::string, std::vector<std::string> > pr = std::make_pair(result, container);
    m_result.push_back(pr);
}

void ResultSet::print(std::ostream & out)
{
    for (int i = 0; i < m_result.size(); i++)
    {
        out << std::endl << std::endl << m_result[i].first << std::endl;
        for (int j = 0; j < m_result[i].second.size(); j++)
        {
            out << ">" << m_result[i].second[j] << std::endl;
        }
    }
}

#endif //ITAK_RESULTSET_H
